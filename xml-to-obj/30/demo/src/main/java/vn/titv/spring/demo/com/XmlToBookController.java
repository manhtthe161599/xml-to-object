package vn.titv.spring.demo.com;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


@RestController
public class XmlToBookController {
    @PostMapping(value = "/xml-to-book", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Book xmlToBook(@RequestParam("xmlFile") MultipartFile xmlFile) {
        try {
            // Kiểm tra file gửi lên có phải là xml không
            if (!xmlFile.getContentType().equals("application/xml")) {
                return null;
            }
            XmlMapper xmlMapper = new XmlMapper();

            // Chuyển đổi XML từ tệp thành đối tượng Book
            Book book = xmlMapper.readValue(xmlFile.getBytes(), Book.class);
            return book;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}

//@RestController
//public class XmlToBookController {
//
//    @PostMapping(value = "/xml-to-book", consumes = MediaType.APPLICATION_XML_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//    public Book xmlToBook(@RequestBody String xmlData) throws Exception {
//        // Tạo một XmlMap để chuyển đổi từ XML sang đối tượng Java
//        XmlMapper xmlMap = new XmlMapper();
//
//        // Chuyển đổi XML thành object
//        Book book = xmlMap.readValue(xmlData, Book.class);
//
//        return book;
//    }
//}


